package models

import (
	"log"
	"time"

	"../config"
)

type Message struct {
	Name      string    `json:"name" bson:"name"`
	Body      string    `json:"body" bson:"body"`
	Channel   string    `json:"channel" bson:"channel"`
	User      string    `json:"user" bson:"user"`
	Timestamp time.Time `json:"timestamp,omitempty" bson:"timestamp"`
}

type SaveMessage struct {
	message *Message
}

func saveMessages(sm *chan SaveMessage) {
	for {
		sM, ok := <-*sm
		if !ok {
			log.Print("Error when receiving message to save")
			return
		}

		err := config.Mgo.DB("").C("messages").Insert(sM.message)
		if err != nil {
			log.Print(err)
		}
	}
}

func NewSaveMessageChan() *chan SaveMessage {
	sm := make(chan SaveMessage, 256)
	go saveMessages(&sm)
	return &sm
}
