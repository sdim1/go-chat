package models

import (
	"log"
	"net/http"

	"../config"
	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
)

type Room struct {
	forward chan *Message
	join chan *Client
	leave chan *Client
	clients map[*Client]bool
}

func run(r *Room) {
	for {
		select {
		case client := <-r.join:
			r.clients[client] = true
		case client := <-r.leave:
			delete(r.clients, client)
			close(client.send)
		case msg := <-r.forward:
			for client := range r.clients {
				client.send <- msg
			}
		}
	}
}

func NewRoomChan() *Room {
	r := &Room{
		forward: make(chan *Message),
		join:    make(chan *Client),
		leave:   make(chan *Client),
		clients: make(map[*Client]bool),
	}
	go run(r)
	return r
}

const (
	socketBufferSize  = 1024
	messageBufferSize = 256
)

var upgrader = &websocket.Upgrader{ReadBufferSize: socketBufferSize,
	WriteBufferSize: socketBufferSize}

func RoomChat(r *Room, sm *chan SaveMessage) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {

		vars := mux.Vars(req)

		socket, err := upgrader.Upgrade(w, req, nil)
		if err != nil {
			log.Print("ServeHTTP:", err)
			return
		}

		session, _ := config.Store.Get(req, "session")
		val := session.Values["user"]
		var user = &User{}
		var ok bool
		if user, ok = val.(*User); !ok {
			log.Print("Invalid session")
			return
		}

		client := &Client{
			socket:  socket,
			send:    make(chan *Message, messageBufferSize),
			room:    r,
			user:    user,
			channel: vars["id"],
			save:    sm,
		}

		r.join <- client
		defer func() { r.leave <- client }()
		go client.write()
		client.read()
	}
}
