package models

import (
	"log"
	"time"

	"github.com/gorilla/websocket"
)

type Client struct {
	channel string
	socket *websocket.Conn
	send chan *Message
	room *Room
	user *User
	save *chan SaveMessage
}

func (c *Client) read() {
	defer c.socket.Close()
	for {
		var msg *Message
		err := c.socket.ReadJSON(&msg)
		if err != nil {
			log.Print(err)
			return
		}

		msg.Name = c.user.Name
		msg.Channel = c.channel
		msg.User = c.user.ID
		msg.Timestamp = time.Now()

		c.room.forward <- msg

		sm := &SaveMessage{
			message: msg,
		}

		*c.save <- *sm
	}
}

func (c *Client) write() {
	defer c.socket.Close()
	for msg := range c.send {
		err := c.socket.WriteJSON(msg)
		if err != nil {
			return
		}
	}
}
