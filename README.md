A test chat app
==============

## Overview

The app contains 2 parts:

- Structs of User, Channel, Message

> There is information about User, Channel & Message from channels holds in struct. When user send messages, data is saved to Mongodb.

- Websocket structs: Client, Room

## Get started

```sh
go get bitbucket.org/sdim1/go-chat
go run main.go
```
