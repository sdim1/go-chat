package config

import "html/template"

var Templ *template.Template

func init() {
	Templ = template.Must(template.ParseGlob("templates/*.html"))
}
