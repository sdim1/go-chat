package controllers

import (
	"encoding/json"
	"log"
	"net/http"

	"../config"
	"../models"
	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
)

func ChannelList(w http.ResponseWriter, r *http.Request) {
	var data []models.Channel
	_ = config.Mgo.DB("").C("channels").Find(nil).All(&data)
	_ = config.Templ.ExecuteTemplate(w, "channel-list.html", data)
}

func channelNew(w http.ResponseWriter, r *http.Request) {

	data := map[string]interface{}{}

	user, ok := r.Context().Value(models.UserKey(0)).(*models.User)

	if r.Method == http.MethodPost && ok {
		channel := models.Channel{}

		err := r.ParseForm()
		if err != nil {
			log.Print(err)
		}

		channel.CreatedBy = user.ID
		channel.ID = bson.NewObjectId()
		channel.Name = r.FormValue("name")

		if channel.Name == "" {
			channel.Name = "No name"
		}

		_ = config.Mgo.DB("").C("channels").Insert(channel)

		data["channel"] = channel
	}

	_ = config.Templ.ExecuteTemplate(w, "channel-new.html", data)
}

func channelView(w http.ResponseWriter, r *http.Request) {
	data := map[string]interface{}{
		"Host": r.Host,
	}

	var channel models.Channel
	vars := mux.Vars(r)
	id := bson.ObjectIdHex(vars["id"])

	_ = config.Mgo.DB("").C("channels").FindId(id).One(&channel)
	data["channel"] = channel

	_ = config.Templ.ExecuteTemplate(w, "channel-view.html", data)
}

func channelHistory(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	const limit = 10
	result := make([]models.Message, limit)

	err := config.Mgo.DB("").C("messages").Find(
		bson.M{"channel": vars["id"]},
	).Sort("-timestamp").Limit(limit).All(&result)

	if err != nil {
		log.Print(err)
	}

	rj, _ := json.Marshal(result)

	w.Header().Set("Content-Type", "application/json")
	_, _ = w.Write(rj)
}

var ChannelNew = MustAuth(channelNew)

var ChannelHistory = MustAuth(channelHistory)

var ChannelView = MustAuth(channelView)
