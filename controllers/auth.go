package controllers

import (
	"context"
	"time"

	"../config"
	"../models"
	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"

	"fmt"
	"net/http"

	"github.com/markbates/goth/gothic"
)

func Index(w http.ResponseWriter, r *http.Request) {
	_ = config.Templ.ExecuteTemplate(w, "index.html", nil)
}

func login(w http.ResponseWriter, r *http.Request) {
	_ = config.Templ.ExecuteTemplate(w, "login.html", nil)
}

func loginHandle(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	action := vars["action"]
	provider := vars["provider"]

	switch action {
	case "login":
		q := r.URL.Query()
		q.Set("provider", provider)

		r.URL.RawQuery = q.Encode()

		config.CreateProvider("https://" + r.Host + "/auth/callback/" + provider)
		gothic.BeginAuthHandler(w, r)

	case "callback":
		gothUser, err := gothic.CompleteUserAuth(w, r)
		if err != nil {
			_, _ = fmt.Fprintln(w, err)
			return
		}

		var user = &models.User{}
		var ID = gothUser.Provider + gothUser.UserID

		_ = config.Mgo.DB("").C("users").Find(
			bson.M{"ID": ID},
		).One(&user)

		if user.ID == "" {
			user.ID = ID
			user.UserID = gothUser.UserID
			user.Provider = gothUser.Provider
			user.Active = true
			user.CreatedAt = time.Now()
		}

		user.Email = gothUser.Email
		user.Name = gothUser.Name
		user.FirstName = gothUser.FirstName
		user.LastName = gothUser.LastName
		user.NickName = gothUser.NickName
		user.Description = gothUser.Description
		user.AvatarURL = gothUser.AvatarURL
		user.Location = gothUser.Location
		user.AccessToken = gothUser.AccessToken
		user.AccessTokenSecret = gothUser.AccessTokenSecret
		user.RefreshToken = gothUser.RefreshToken
		user.ExpiresAt = gothUser.ExpiresAt

		// Update or insert new user
		_, _ = config.Mgo.DB("").C("users").UpsertId(ID, user)

		session, _ := config.Store.Get(r, "session")
		session.Values["user"] = user
		_ = session.Save(r, w)

		w.Header().Set("Location", "/channel")
		w.WriteHeader(http.StatusTemporaryRedirect)

	default:
		w.WriteHeader(http.StatusNotFound)
		_, _ = fmt.Fprintf(w, "Auth action %s not supported", action)
	}
}

func logout(w http.ResponseWriter, r *http.Request) {
	session, _ := config.Store.Get(r, "session")
	session.Options.MaxAge = -1
	_ = session.Save(r, w)

	http.Redirect(w, r, "/", http.StatusSeeOther)
}

func MustAuth(handlerFunc http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		session, err := config.Store.Get(r, "session")
		if err != nil {
			fmt.Println(err)
		}

		val := session.Values["user"]
		user, ok := val.(*models.User)

		if !ok {
			// not authenticated
			w.Header().Set("Location", "/auth/login")
			w.WriteHeader(http.StatusTemporaryRedirect)
			return
		}

		ctx := r.Context()
		ctx = context.WithValue(ctx, models.UserKey(0), user)

		// success - call the original handler
		handlerFunc(w, r.WithContext(ctx))
	}
}

func MustNotAuth(handlerFunc http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		session, err := config.Store.Get(r, "session")
		if err != nil {
			fmt.Println(err)
		}

		val := session.Values["user"]
		if _, ok := val.(*models.User); ok {
			// already authenticated
			w.Header().Set("Location", "/")
			w.WriteHeader(http.StatusTemporaryRedirect)
			return
		}

		// success - call the original handler
		handlerFunc(w, r)
	}
}

// Logout uses to log User out
var Logout = MustAuth(logout)

// LoginHandle redirect user to providers' login page & receive callback from them
var LoginHandle = MustNotAuth(loginHandle)

// Login servers our login page
var Login = MustNotAuth(login)
